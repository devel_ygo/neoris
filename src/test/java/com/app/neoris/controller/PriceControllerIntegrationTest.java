package com.app.neoris.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
@SpringBootTest
@AutoConfigureMockMvc
class PriceControllerIntegrationTest {
    public static final String PATH_ENDPOINT_GET = "/prices/price/{inDate}/{idProduct}/{idBrand}";
    private static final String ERROR_CONTACT_ADMIN = "Error, please contact admin.";
    @Autowired
    private MockMvc mockMvc;
    private static ObjectMapper objectMapper;
    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @ParameterizedTest
    @CsvSource({"2020-06-14 10:00:00,35455,1,35.50",
                "2020-06-14 16:00:00,35455,1,25.45",
                "2020-06-14 21:00:00,35455,1,35.50",
                "2020-06-15 10:00:00,35455,1,30.50",
                "2020-06-16 21:00:00,35455,1,38.95"})
    void findPriceOkTest(String date, Long idProduct, Long idBrand, Double price) throws Exception {
        // Arrange
        // Act
        ResultActions response = mockMvc.perform(get(PATH_ENDPOINT_GET,date, idProduct, idBrand)
                .contentType(MediaType.APPLICATION_JSON));
                // Assert
        response.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idBrand").value(idBrand))
                .andExpect(jsonPath("$.price").value(price));
    }

    @ParameterizedTest
    @CsvSource({"2020-06-14 10:00:00,35456,1,El producto 35456 no existe.",
                "2020-06-14 16:00:00,35455,3,La marca 3 no existe.",
                "2020-06-10 21:00:00,35455,1,No se encontraron registros del precio."})
    void findPriceBadResquestTest(String date, Long idProduct, Long idBrand, String message) throws Exception {
        // Arrange
        // Act
        ResultActions response = mockMvc.perform(get(PATH_ENDPOINT_GET, date, idProduct, idBrand)
                        .contentType(MediaType.APPLICATION_JSON));
        // Assert
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(message))
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @ParameterizedTest
    @CsvSource({"2020-06-14 10:00:0,35455,1"})
    void findPriceGetError500tTest(String date, Long idProduct, Long idBrand) throws Exception {
        // Arrange
        // Act
        ResultActions response = mockMvc.perform(get(PATH_ENDPOINT_GET, date, idProduct, idBrand)
                .contentType(MediaType.APPLICATION_JSON));
        // Assert
        response.andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.message").value(ERROR_CONTACT_ADMIN))
                .andReturn()
                .getResponse()
                .getContentAsString();
    }

    @AfterAll
    static void afterAll() {
        objectMapper = null;
    }
}