package com.app.neoris.controller;

import com.app.neoris.exceptions.BusinessException;
import com.app.neoris.model.dto.IntDto;
import com.app.neoris.model.dto.OutDto;
import com.app.neoris.services.PriceServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(PriceController.class)
class PriceControllerTest {
    public static final String PATH_ENDPOINT_GET = "/prices/price/{inDate}/{idProduct}/{idBrand}";
    public static final String DATE_IN = "2024-06-14 00:00:00";
    public static final long ID_PRODUCT = 1L;
    public static final long ID_BRAND = 1L;
    public static final String PRICE = "34.67";
    public static final String NO_SE_ENCONTRARON_REGISTROS_DEL_PRECIO = "No se encontraron registros del precio.";
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private PriceServices services;
    private ObjectMapper objectMapper;
    private IntDto intDto = null;

    private OutDto outDataDto = null;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        outDataDto = OutDto.builder().idBrand(ID_BRAND).idProduct(ID_PRODUCT).priceList(1).startDate(null).endDate(null).price(new BigDecimal(PRICE)).build();
    }

    @Test
    void findPriceOkTest() throws Exception {
        // Arrange
        when(services.findByPrice(anyString(), anyLong(), anyLong())).thenReturn(outDataDto);
        // Act
        ResultActions response = mockMvc.perform(get(PATH_ENDPOINT_GET,DATE_IN, ID_PRODUCT, ID_BRAND)
                                        .contentType(MediaType.APPLICATION_JSON));
                // Assert
        response.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.idBrand").value(ID_BRAND))
                .andExpect(jsonPath("$.price").value(PRICE))
        ;
        verify(services).findByPrice(anyString(), anyLong(), anyLong());
    }

    @Test
    void findPriceBadResquestTest() throws Exception {
        // Arrange
        doThrow(new BusinessException(NO_SE_ENCONTRARON_REGISTROS_DEL_PRECIO)).when(services).findByPrice(anyString(), anyLong(), anyLong());
        // Act
        ResultActions response = mockMvc.perform(get(PATH_ENDPOINT_GET,DATE_IN, ID_PRODUCT, ID_BRAND)
                                        .contentType(MediaType.APPLICATION_JSON));
        // Assert
        response.andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value(NO_SE_ENCONTRARON_REGISTROS_DEL_PRECIO))
                .andReturn()
                .getResponse()
                .getContentAsString();

        verify(services).findByPrice(anyString(), anyLong(), anyLong());
    }
}