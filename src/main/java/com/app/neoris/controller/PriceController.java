package com.app.neoris.controller;

import com.app.neoris.model.dto.OutDto;
import com.app.neoris.services.IPrice;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@RequestMapping(path = "/prices")
@Api(value = "Get price Neoris")
public class PriceController {
    private final IPrice iPrice;
    public PriceController(IPrice iPrice) {
        this.iPrice = iPrice;
    }
    @GetMapping(value = "/price/{inDate}/{idProduct}/{idBrand}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.OK)
    @ApiOperation(value = "Get price", notes = "Get the price with highest priority." )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK. If the price exist.", response = Map.class ),
            @ApiResponse(code = 400, message = "Bad Request. If not exist the price", response = Map.class),})
    public ResponseEntity<OutDto> findPrice(@PathVariable (required = true, name = "inDate") String inDate,
                                       @PathVariable (required = true, name = "idProduct") Long idProduct,
                                       @PathVariable (required = true, name = "idBrand") Long idBrand) throws Exception {

        log.info("param inDate {} idProduct {} idBrand {} ", inDate, idProduct, idBrand);
        OutDto outDto = iPrice.findByPrice(inDate, idProduct, idBrand);
        log.info("param OutDto {}", outDto.toString());
        return ResponseEntity.ok(outDto);
    }
}
