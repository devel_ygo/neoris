package com.app.neoris.generic;

import com.app.neoris.exceptions.BusinessException;

public interface IGenericDao <R> {
    R findByPrice(String date, Long idProduct, Long idBrand) throws BusinessException;
}

