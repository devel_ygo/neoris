package com.app.neoris.repository;

import com.app.neoris.model.entity.Price;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;
@Repository
public interface PriceRepository extends JpaRepository <Price, Long> {

    @Query(value = "SELECT * FROM prices WHERE ?1 BETWEEN start_date AND end_date AND brand_id = ?2 AND product_id = ?3 ORDER BY priority DESC LIMIT 1", nativeQuery = true)
    Optional<Price> getPrice(LocalDateTime dateTime, Long idBrand, Long idProduct);
}
