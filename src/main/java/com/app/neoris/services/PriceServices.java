package com.app.neoris.services;

import com.app.neoris.exceptions.BusinessException;
import com.app.neoris.model.dto.IntDto;
import com.app.neoris.model.dto.OutDto;
import com.app.neoris.model.entity.Price;
import com.app.neoris.repository.BrandRepository;
import com.app.neoris.repository.PriceRepository;
import com.app.neoris.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
@Service
@AllArgsConstructor
public class PriceServices implements IPrice {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private final PriceRepository priceRepository;
    private final BrandRepository brandRepository;
    private final ProductRepository productRepository;
    @Override
    public OutDto findByPrice(String date, Long idProduct, Long idBrand) throws BusinessException {
        OutDto outDto = new OutDto();
        IntDto intDto = new IntDto(date,idProduct,idBrand);

        brandRepository.findById(intDto.getIdBrand()).orElseThrow(() ->{
            log.error(String.format("La marca %s no existe.", intDto.getIdBrand()));
            return new BusinessException(String.format("La marca %s no existe.", intDto.getIdBrand()));
        });

        productRepository.findById(intDto.getIdProduct()).orElseThrow(()-> {
            log.error(String.format("El producto %s no existe.", intDto.getIdProduct()));
            return new BusinessException(String.format("El producto %s no existe.", intDto.getIdProduct()));
        });

        log.info("Intentando hacer la consulta....! {}",intDto.toString());
        LocalDateTime dateTime = LocalDateTime.parse(intDto.getDateIn(), FORMATTER);

        Price price = priceRepository.getPrice(dateTime, intDto.getIdBrand(),intDto.getIdProduct())
                .orElseThrow(() -> new BusinessException("No se encontraron registros del precio."));

        log.info("Finalizo de hacer la consulta....! {}",price.toString());
        return outDto.priceToOutDto(price);
    }
}
