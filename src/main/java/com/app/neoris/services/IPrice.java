package com.app.neoris.services;

import com.app.neoris.exceptions.BusinessException;
import com.app.neoris.generic.IGenericDao;
import com.app.neoris.model.dto.OutDto;
public interface IPrice extends IGenericDao<OutDto> {
    OutDto findByPrice(String date, Long idProduct, Long idBrand) throws BusinessException;
}
