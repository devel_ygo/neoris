package com.app.neoris.constant;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum Currency {
    EUR ("EURO","EUR","$"),
    DOLLAR ("DOLLAR","DOL","$");

    private final String name;
    private final String nick;
    private final String signal;
}
