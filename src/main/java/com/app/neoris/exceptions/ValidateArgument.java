package com.app.neoris.exceptions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateArgument {

    private ValidateArgument() {
    }

    public static void obligateValue(Object value, String text) {
        if (value == null || value.toString().isEmpty()) {
            throw new ExceptionObligateValue(text);
        }
    }

    public static void formatValidate(Object value, String pattern, String text) {
        Pattern pat = Pattern.compile(pattern);
        Matcher mat = pat.matcher(value.toString());
        if (!mat.matches()) {
            throw new BusinessException(text);
        }
    }
}