package com.app.neoris.exceptions;

import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;
@Getter
@ToString
public class ErrorDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private final String message;
    public ErrorDto(String message) {
        this.message = message;
    }
}