package com.app.neoris.exceptions;

public class ExceptionObligateValue extends RuntimeException {
	private static final long serialVersionUID = 1L;
	public ExceptionObligateValue(String text) {
        super(text);
    }
}
