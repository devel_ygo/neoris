package com.app.neoris.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ExceptionHandlerExceptionResolver;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ConcurrentHashMap;
@Slf4j
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {
    private static final String ERROR_CONTACT_ADMIN = "Error, please contact admin.";
    private static final ConcurrentHashMap<String, Integer> STATE_CODE = new ConcurrentHashMap<>();
    public ErrorHandler (){
        STATE_CODE.put(IllegalArgumentException.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        STATE_CODE.put(ExceptionObligateValue.class.getSimpleName(), HttpStatus.BAD_REQUEST.value());
        STATE_CODE.put(BusinessException.class.getSimpleName(),HttpStatus.BAD_REQUEST.value());
        STATE_CODE.put(HttpMessageNotReadableException.class.getSimpleName(),HttpStatus.BAD_REQUEST.value());
        STATE_CODE.put(ExceptionHandlerExceptionResolver.class.getSimpleName(),HttpStatus.BAD_REQUEST.value());
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Error> handleAllExceptions(Exception exception, HttpServletRequest response) {
        ResponseEntity<Error> result;

        String name = exception.getClass().getSimpleName();
        String message = exception.getMessage();
        Integer code = STATE_CODE.get(name);
        log.info("Code error name {} message {} code {}", name, message, code);

        if (code != null) {
            ErrorDto error = new ErrorDto(message);
            result = new ResponseEntity(error, HttpStatus.valueOf(code));
        } else {
            log.error(message, exception);
            ErrorDto error = new ErrorDto(ERROR_CONTACT_ADMIN);
            result = new ResponseEntity(error, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return result;
    }
}
