package com.app.neoris.exceptions;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Getter
public class ApiException extends Exception {

    private static final long serialVersionUID = 1L;

    private final String code;

    private final int responseCode;

    private final List<ErrorDto> errorList = new ArrayList<>();

    public ApiException(String code, int responseCode, String message) {
        super(message);
        log.info("code {} message {} HttpStatus.BAD_REQUEST.value() {}", code, message, HttpStatus.BAD_REQUEST.value());
        this.code = code;
        this.responseCode = responseCode;
    }

    public ApiException(String code, int responseCode, String message, List<ErrorDto> errorList) {
        super(message);
        this.code = code;
        this.responseCode = responseCode;
        this.errorList.addAll(errorList);
    }
}
