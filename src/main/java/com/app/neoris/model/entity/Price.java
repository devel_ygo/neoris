package com.app.neoris.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
@Entity
@Table (name = "PRICES")
public class Price implements Serializable {
    @Id
    @Column (name = "ID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column (name = "BRAND_ID")
    private int brandId;
    @Column (name = "START_DATE")
    private LocalDateTime startDate;
    @Column (name = "END_DATE")
    private LocalDateTime endDate;
    @Column (name = "PRICE_LIST")
    private int priceList;
    @Column (name = "PRODUCT_ID")
    private int product;
    @Column (name = "PRIORITY")
    private int priority;
    @Column (name = "PRICE")
    private BigDecimal value;
    @Column (name = "CURR")
    private String curr;
}
