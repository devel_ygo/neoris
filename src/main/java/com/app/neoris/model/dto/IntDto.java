package com.app.neoris.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

import static com.app.neoris.exceptions.ValidateArgument.obligateValue;
@Data
@Slf4j
@NoArgsConstructor
public class IntDto implements Serializable {
    private static final String REQUIRED_FIELD = "The field %s it's obligate";
    private String dateIn;
    private Long idProduct;
    private Long idBrand;

    public IntDto(String dateIn, Long idProduct, Long idBrand) {
        obligateValue(dateIn, String.format(REQUIRED_FIELD, "dateIn"));
        obligateValue(idProduct, String.format(REQUIRED_FIELD, "idProduct"));
        obligateValue(idBrand, String.format(REQUIRED_FIELD, "idBrand"));

        this.dateIn = dateIn;
        this.idProduct = idProduct;
        this.idBrand = idBrand;
    }
}
