package com.app.neoris.model.dto;

import com.app.neoris.model.entity.Price;
import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@ToString
public class OutDto implements Serializable {
    private long idBrand;
    private long idProduct;
    private long priceList;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private BigDecimal price;


    public OutDto priceToOutDto (Price price){
        return OutDto.builder()
                .idBrand(price.getBrandId())
                .idProduct(price.getProduct())
                .priceList(price.getPriceList())
                .startDate(price.getStartDate())
                .endDate(price.getEndDate())
                .price(price.getValue())
                .build();
    }
}
