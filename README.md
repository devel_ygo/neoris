### NEORIS
#### _Aplicación Backend test neoris._
#### Requerimientos.
* Java 1.8.
* SpringBoot 2.7.
* Gradle 7.5.
* JUnit 5.
* H2

#### _El motor de base datos H2 requiere de los siguientes parametros:_
 - `ENV=dev` 
 - `CONTEXT_PATH=/neoris;`
 - `PORT=8090;`
 - `URL_DATABASE=jdbc:h2:mem:neoris;DATABASE_TO_UPPER=false;DB_CLOSE_ON_EXIT=FALSE;`
 - `USER_DATABASE=test`
 - `PASSWORD_DATABASE=test` 
 
##### _Recursos_
 - Propiedad para definir el perfil =`neoris/src/main/resources/application.properties`
 - Propiedad perfil (dev) =`neoris/src/main/resources/application-dev.properties`
 - Propiedad perfil (pdn) =`neoris/src/main/resources/application-pdn.properties`
 - Sritps SQL  =`neoris/src/main/resources/import.sql`
######
 - server.port = ${PORT}
 - server.servlet.context-path = ${CONTEXT_PATH}
 - spring.h2.console.path=/h2-console
 - spring.h2.console.enabled=true
 - spring.datasource.driverClassName: org.h2.Driver
 - spring.datasource.url=${URL_DATABASE}
 - spring.datasource.username=${USER_DATABASE}
 - spring.datasource.password=${PASSWORD_DATABASE}

#### _Puntos de acceso._
Este proyecto esta creado en springboot, por lo cual ya cuenta con un servidor por defecto en cual se puede desplegar
con solo iniciandolo desde un ide como SpringTools o Intellij IDEA desde la clase `NeorisApplication.java` , o acceder desde
el dominio definido y posterior a su despliegue se puede acceder a los endpoints desde la url
`{{server_local}}:{{port}}{{context_path}}`

1. *Se encuentra el recurso el metodo GET`/prices/price/`.*
   - Parámetros de entrada: 
     - Fecha (yyyy-MM-dd HH:mm:ss) = 2020-06-15 21:00:00
     - Id producto                 = 35455
     - Id brand                    = 1
     se debería usar así:
     `{{server_local}}:{{port}}{{context_path}}neoris/prices/price/2020-06-15%2021%3A00%3A00/35455/1`

    - Como respuesta satisfactorio será:
      `{
      "idBrand": 1,
      "idProduct": 35455,
      "priceList": 4,
      "startDate": "2020-06-15T16:00:00",
      "endDate": "2020-12-31T23:59:59",
      "price": 38.95
      }`
   
    - Código de respuesta : 200
    - Descripción         : OK
    - Encabezados de respuesta:
      `connection: keep-alive
      content-type: application/json
      date: Tue18 Jul 2023 13:38:09 GMT
      keep-alive: timeout=60
      transfer-encoding: Identity `
    
    - Como respuesta incorrecto será:
      `{
      "message": "Error, please contact admin."
      }`
    - Código de respuesta : 400
    - Descripción         : Bad Request
   - Encabezados de respuesta:
     `connection: close
     content-type: application/json
     date: Tue18 Jul 2023 13:32:57 GMT
     transfer-encoding: Identity`
    
    - también se puede intentar con el comando Curl
      `curl -X GET "http://localhost:8090/neoris/prices/price/2020-06-15%2021%3A00%3A00/35455/1" -H "accept: application/json"`

- #### **_NOTA:_** Se puede obtener mas información en la url de Swagger:
    * Información Xml `{{server_local}}:{{port}}{{context_path}}/v2/api-docs`
    * Información grafica `{{server_local}}:{{port}}{{context_path}}/swagger-ui/`
